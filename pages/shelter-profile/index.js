import React, { useState } from 'react';
import { FiArrowLeft } from 'react-icons/fi';
import Link from 'next/link';

import styles from './shelter-profile.module.css';

export default function Index() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [whatsapp, setWhatsapp] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');

    function handleUpdate(e) {
        e.preventDefault();
        const data = {
            name,
            email,
            whatsapp,
            city,
            state
        }
        console.log('button clicked', data);
    }
    return(
        <div className={styles.profile_container}>
            <div className={styles.content}>
                <section>
                    <img src="/static/lineartext.png" alt="saveIt"/>
                    <img src="/static/flying.svg" alt="hero" style={{height: "200px"}}/>
                    <p>
                        <Link href="/cases">
                            <a className="back-link"><FiArrowLeft size={16} color="#ff0054" /> 
                            Back to Home</a>
                        </Link>
                    </p>
                </section>
                <form onSubmit={handleUpdate}>
                    <h1>Profile</h1>
                    <input 
                        type="text" 
                        placeholder="Name"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />  
                    <input 
                        type="email" 
                        placeholder="Email" 
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    /> 
                    <input 
                        type="text" 
                        placeholder="Whatsapp" 
                        value={whatsapp}
                        onChange={e => setWhatsapp(e.target.value)}
                    /> 
                    <input 
                        type="text" 
                        placeholder="City" 
                        value={city}
                        onChange={e => setCity(e.target.value)}
                    /> 
                    <input 
                        type="text" 
                        placeholder="State" 
                        maxLength="2" 
                        value={state}
                        onChange={e => setState(e.target.value)}
                    /> 
                    <input type="submit" className="button" value="Update" />
                </form>
            </div>
        </div>
    );
}