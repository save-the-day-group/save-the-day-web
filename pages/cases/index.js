import React from 'react';
import { useRouter } from 'next/router';
import { FiPower, FiUser, FiTrash2 } from 'react-icons/fi';

import styles from './cases.module.css';

export default function Index() {
    const router = useRouter();
    function goToNewCase() {
       router.push('/new-case');
    }
    function goToProfile() {
        console.log('button clicked!');
        router.push('/shelter-profile');
    }

    function logout() {
        router.push('/');
    }
    return(
        <div className={styles.cases_container}>
            <header>
                <img src="/static/lineartext.png" style={{height: "100px"}} alt="saveIt"/>
                <span>Welcome, Shelter</span>
                <button 
                    className={styles.left,styles.newCaseBtn} 
                    style={{maxWidth: "200px", letterSpacing: "0em", marginTop: "-2px"}}
                    onClick={goToNewCase}
                >
                    Create New Case
                </button>
                <button className="profile-btn" onClick={goToProfile}>
                    <FiUser size={15} style={{marginRight: "8px"}} />
                    Profile
                </button>
                <button className={styles.btn} onClick={logout}>
                    <FiPower size={16} />
                    Logout
                </button>
            </header>
            <h1>Registered Cases</h1>
            <ul>
                <li>
                    <strong>CASE:</strong>  
                    <p>Kitties on the street</p>

                    <strong>DESCRIPTION:</strong>  
                    <p>We need to take the kitties to the vet.</p>

                    <strong>VALUE:</strong>  
                    <p>Any value will help!</p>
                    <button className={styles.delete}>
                        <FiTrash2 size={20} /> 
                    </button>
                </li>
            </ul>
            
        </div>
    );
}