import React, { useState } from 'react';
import { FiArrowLeft } from "react-icons/fi";
import Link from 'next/link';

import styles from './register.module.css';

export default function Index() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [whatsapp, setWhatsapp] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [password, setPassword] = useState('');


    function handleRegister(e) {
        e.preventDefault();
        const data = {
            name,
            email,
            whatsapp,
            city,
            state,
            password
        }
        console.log('Signup clicked', data);
    }

    return(
        <div className={styles.register_container}>
            <div className={styles.page_content}>
                <section>
                    <img src="/static/lineartext.png" alt="save" />
                    <img src="/static/flying.svg" alt="hero" style={{height: "200px", color:"#ff0054"}}/>
                    <p>Create a new account and be visible so that people can help your shelter.</p>
                    <Link href="/">
                        <a className="back-link"><FiArrowLeft size={16} color="#ff0054" />
                        Already have an account?</a>
                    </Link>
                </section>
                <form onSubmit={handleRegister}>
                <h1>Create Account</h1>
                    <input 
                        type="text" 
                        placeholder="Name" 
                        value={name} 
                        onChange={e => setName(e.target.value)} 
                    />
                    <input 
                        type="email" 
                        placeholder="Email" 
                        value={email} 
                        onChange={e => setEmail(e.target.value)} 
                    />
                    <input 
                        type="text" 
                        placeholder="WhatsApp" 
                        value={whatsapp} 
                        onChange={e => setWhatsapp(e.target.value)} 
                    />
                    <div className={styles.group_input}>
                        <input 
                            type="text" 
                            placeholder="City" 
                            value={city}
                            onChange={e => setCity(e.target.value)}  
                        />
                        <input 
                            type="text" 
                            placeholder="State" 
                            style={{ width: 100 }} 
                            maxLength="2"
                            value={state}
                            onChange={e => setState(e.target.value)} 
                        />
                    </div>
                    <input 
                        type="password" 
                        placeholder="Choose password" 
                        value={password} 
                        onChange={e => setPassword(e.target.value)} 
                    />
                    <button type="submit" className="button">Register</button>
                </form>
            </div>
        </div>
    );
}