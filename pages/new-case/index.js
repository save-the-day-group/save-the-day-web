import React, { useState } from 'react';
import { FiArrowLeft } from "react-icons/fi";
import Link from 'next/link';

import styles from './new-case.module.css';

export default function Index() {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [value, setValue] = useState('');

    function handleSubmit(e) {
        e.preventDefault();
        const data = {
            title,
            description,
            value
        }
        console.log('button clicked', data);
    }
    return(
        <div className={styles.new_case_container}>
            <div className={styles.content}>
                <section>
                    <img src="/static/lineartext.png" alt="saveIt" style={{ marginLeft: "24px" }} />
                    <h1>Create a new Case</h1>
                    <p>Describe the case and how people can help.</p>
                    <div>
                        <Link href="/cases">
                           <a className="back-link"><FiArrowLeft size={16} color="#ff0054" />
                            Back to Home</a>
                        </Link>
                    </div>
                </section>
                <form onSubmit={handleSubmit}>
                    <input 
                        type="text" 
                        placeholder="Title" 
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                    />
                    <textarea 
                        type="text" 
                        placeholder="Description" 
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                    />
                    <input 
                        type="text" 
                        placeholder="How to help" 
                        value={value}
                        onChange={e => setValue(e.target.value)}
                    />
                    <input type="submit" className="button" value="Create Case"/>
                </form>
            </div>
        </div>
    );
}