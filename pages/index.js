import Head from "next/head";
import React, { useState } from 'react';
import Link from 'next/link';

import { FiLogIn } from 'react-icons/fi';

import styles from './index.module.css'

export default function Home() {
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');

  //const history = useHistory();

  function handleLogin(e) {
      e.preventDefault();
      const data = {
          email,
          password
      }
      console.log('login clicked', data);
      history.push('/cases');
  }
  return (
    <div>
      {/* Head is the page title, the one that will appear on browser tab. It has a title and a favicon */}
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className={styles.login_container}>
          <div className={styles.page_content}>
            <section>
              <h1>Shelter Login</h1>
              <form onSubmit={handleLogin}>
                <input
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <input
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <button type="submit" className="button">
                  Login
                </button>
              </form>
              <Link href="/register">
                <a className="back-link"><FiLogIn size={16} color="#ff0054" />
                No account? Register here</a>
              </Link>
            </section>
          </div>
          <div className={styles.img_content}>
            <img src="/static/lineartext.png" alt="logo " />
            <img src="/static/flying.svg" alt="Hero" style={{ height: "200px" }} />
          </div>
        </div>
      </main>

      <footer></footer>
    </div>
  );
}
